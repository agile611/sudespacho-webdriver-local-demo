package com.itnove.sudespacho.tests;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

/**
 * Created by guillemhs on 2018-01-22.
 */
public class WebDriverTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public Actions hover;
    public int timeOut = 10;

    @BeforeMethod
    public void setUp() throws IOException {
        String browser = System.getProperty("browser");
        String hub = System.getProperty("hub");
        if (browser != null && browser.equalsIgnoreCase("firefox")) {
            FirefoxOptions capabilities = new FirefoxOptions();
            System.setProperty("webdriver.gecko.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver-linux");
            driver = new RemoteWebDriver(new URL(hub), capabilities);
        } else {
            ChromeOptions capabilities = new ChromeOptions();
            System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver-linux");
            driver = new RemoteWebDriver(new URL(hub), capabilities);
        }
        wait = new WebDriverWait(driver, timeOut);
        hover = new Actions(driver);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
        //Accedir a pagina
        driver.navigate().to("http://www.iberley.es");
    }

    @Test
    public void webDriverTestDemo() throws InterruptedException {
        for (int i = 1; i < 20; i++) {
            driver.get("http://www.iberley.es");
            driver.findElement(By.id("form_term")).clear();
            driver.findElement(By.id("form_term")).sendKeys("bitcoin");
            driver.findElement(By.id("form_save")).click();
            Thread.sleep(3000);
            assertTrue(false);
        }
    }

    @AfterMethod
    public void tearDown(ITestResult testResult) throws IOException {
        Date timestamp = new Date();
        if (testResult.getStatus() == ITestResult.FAILURE) {
            System.out.println(testResult.getStatus());
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(testResult.getName() + "-" + timestamp.getTime() + ".jpg"));
        }
        driver.quit();

    }
}
